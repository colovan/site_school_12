import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import {
  library
} from '@fortawesome/fontawesome-svg-core'
import {
  faSchool,
  faSquareFull,
  faEye,
  faChevronRight,
  faChevronDown,
  faBars,
  faImage
} from '@fortawesome/free-solid-svg-icons'
import {
  faInstagram,
  faOdnoklassnikiSquare
} from '@fortawesome/free-brands-svg-icons'
import {
  FontAwesomeIcon
} from '@fortawesome/vue-fontawesome'
import '@/assets/styles/style.css'
import '@/assets/element-variables.scss'
import axios from "axios"
import VueYandexMetrika from 'vue-yandex-metrika' 
import VueScrollTo from "vue-scrollto"

Vue.use(VueScrollTo, {
  container: "body",
  duration: 0,
  easing: "ease",
  offset: 0,
  force: true,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
})
Vue.use(VueYandexMetrika, {
  id: 65077753,
  router: router,
  env: process.env.NODE_ENV
})

const settings = {
  apiKey: '',
  lang: 'ru_RU',
  coordorder: 'latlong',
  version: '2.1'
}

Vue.config.productionTip = false
Vue.use(element, {
  locale
})
library.add(faInstagram, faOdnoklassnikiSquare, faEye, faSchool, faSquareFull, faChevronRight, faChevronDown, faBars, faImage)

Vue.component('font-awesome-icon', FontAwesomeIcon)

router.beforeEach((to, from, next) => {
  store.commit('changeLoading', true)
  if (
    localStorage.getItem("token") === undefined ||
    localStorage.getItem("token") == ""
  ) {
    store.commit("changeIsAuth", false);
    if (to.name.indexOf('Admin') != -1 && to.name != 'AdminAuth') {
      next({
        name: 'AdminAuth'
      })
    } else {
      next()
    }
    store.commit('changeLoading', false)
  } else {
    axios({
        method: "get",
        url: "/api/sign",
        headers: {
          'Token': localStorage.getItem('token')
        }
      }).then(res => {
        if (res.status == 200) {
          store.commit("changeIsAuth", true);
          if (to.name.indexOf('Admin') != -1 && to.name != 'AdminAuth') {
            axios({
              mehtod: 'get',
              url: '/api/sign/update',
              headers: {
                'Token': localStorage.getItem('token')
              }
            }).then(res => {
              store.commit('changeLoading', false)

              next()
            })
          } else {
            store.commit('changeLoading', false)

            next()
          }
        }
      })
      .catch(error => {
        store.commit("changeIsAuth", false);
        if (to.name.indexOf('Admin') != -1 && to.name != 'AdminAuth') {
          store.commit('changeLoading', false)
          next({
            name: 'AdminAuth'
          })
        } else {
          store.commit('changeLoading', false)
          next()

        }
      });
  }
})

new Vue({
  router,
  store,
  render: function (h) {
    return h(App)
  }
}).$mount('#app')