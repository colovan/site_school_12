import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    newItemData: {},
    docItemData: {},
    bookItemData: {},
    workerItemData: {},
    isAuth: false,
    isBadEye: false,
    isMenuBadEye: true,
    loading: false
  },
  mutations: {
    changeNewItemData (state, value) {
      state.newItemData = value
    },
    changeDocItemData (state, value) {
      state.docItemData = value
    },
    changeBookItemData (state, value) {
      state.bookItemData = value
    },
    changeWorkerItemData (state, value) {
      state.workerItemData = value
    },
    changeIsAuth (state, value) {
      state.isAuth = value
    },
    changeIsBadEye (state, value) {
      state.isBadEye = value
    },
    changeIsMenuBadEye (state, value) {
      state.isMenuBadEye = value
    },
    changeLoading(state, value) {
      state.loading = value
    }
  },
  actions: {
  },
  modules: {
  }
})