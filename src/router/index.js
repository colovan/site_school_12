import Vue from 'vue'
import VueRouter from 'vue-router'
import News from '@/views/NavElements/News.vue'
import PageNew from '@/views//PageNew.vue'
import NavMobile from '@/views//NavMobile.vue'
import BadEyeNavMenu from '@/views/BadEyeNavMenu.vue'
import DistanceLearning from '@/views/NavElements/DistanceLearning.vue'

import General from '@/views/NavElements/school_info/General.vue'
import Collective from '@/views/NavElements/school_info/Collective.vue'
import Education from '@/views/NavElements/school_info/Education.vue'
import MaterialTechnicGiving from '@/views/NavElements/school_info/MaterialTechnicGiving.vue'
import MethodGivingStudy from '@/views/NavElements/school_info/MethodGivingStudy.vue'
import SecurityAntiterror from '@/views/NavElements/school_info/SecurityAntiterror.vue'

import OtherVictory from '@/views/NavElements/victory/OtherVictory.vue'
import SportVictory from '@/views/NavElements/victory/SportVictory.vue'
import StudentYearVictory from '@/views/NavElements/victory/StudentYearVictory.vue'

import HotLines from '@/views/NavElements/useful/HotLines.vue'
import InfoSecurity from '@/views/NavElements/useful/InfoSecurity.vue'
import LinksUse from '@/views/NavElements/useful/LinksUse.vue'

import Docs from '@/views/NavElements/Docs.vue'

import SchoolPaper from '@/views/NavElements/SchoolPaper.vue'

import LibGeneral from '@/views/NavElements/liblary/General.vue'
import ActualBooks from '@/views/NavElements/liblary/ActualBooks.vue'

import AdminAuth from '@/views/Admin/AdminAuth.vue'
import Admin from '@/views/Admin/Admin.vue'

import AdminDocs from '@/views/Admin/AdminDocs.vue'
import AdminDocCreate from '@/views/Admin/AdminDocCreate.vue'
import AdminDocUpdate from '@/views/Admin/AdminDocUpdate.vue'

import AdminNews from '@/views/Admin/AdminNews.vue'
import AdminNewCreate from '@/views/Admin/AdminNewCreate.vue'
import AdminNewUpdate from '@/views/Admin/AdminNewUpdate.vue'

import AdminWorkers from '@/views/Admin/AdminWorkers.vue'
import AdminWorkerCreate from '@/views/Admin/AdminWorkerCreate.vue'
import AdminWorkerUpdate from '@/views/Admin/AdminWorkerUpdate.vue'


import AdminBooks from '@/views/Admin/AdminBooks.vue'
import AdminBookCreate from '@/views/Admin/AdminBookCreate.vue'
import AdminBookUpdate from '@/views/Admin/AdminBookUpdate.vue'








Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Main',
    component: News
  },
  {
    path: '/navmobile',
    name: 'NavMobile',
    component: NavMobile
  },
  {
    path: '/BadEyeNavMenu',
    name: 'BadEyeNavMenu',
    component: BadEyeNavMenu
  },
  {
    path: '/adminauth',
    name: 'AdminAuth',
    component: AdminAuth
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin
  },
  {
    path: '/admindocs',
    name: 'AdminDocs',
    component: AdminDocs
  },
  {
    path: '/adminnews',
    name: 'AdminNews',
    component: AdminNews
  },
  {
    path: '/adminworkers',
    name: 'AdminWorkers',
    component: AdminWorkers
  },
  {
    path: '/adminbooks',
    name: 'AdminBooks',
    component: AdminBooks
  },
  {
    path: '/admindoccreate',
    name: 'AdminDocCreate',
    component: AdminDocCreate
  },
  {
    path: '/adminnewcreate',
    name: 'AdminNewCreate',
    component: AdminNewCreate
  },
  {
    path: '/adminbookcreate',
    name: 'AdminBookCreate',
    component: AdminBookCreate
  },
  {
    path: '/adminworkercreate',
    name: 'AdminWorkerCreate',
    component: AdminWorkerCreate
  },
  {
    path: '/admindocupdate',
    name: 'AdminDocUpdate',
    component: AdminDocUpdate
  },
  {
    path: '/adminBookupdate',
    name: 'AdminBookUpdate',
    component: AdminBookUpdate
  },
  {
    path: '/adminnewupdate',
    name: 'AdminNewcUpdate',
    component: AdminNewUpdate
  },
  {
    path: '/AdminWorkerUpdate',
    name: 'AdminWorkerUpdate',
    component: AdminWorkerUpdate
  },
  {
    path: '/news',
    name: 'News',
    component: News
  },
  {
    path: '/newPage/:id',
    name: 'NewPage',
    component: PageNew
  },
  {
    path: '/distanceLearning',
    name: 'DistanceLearning',
    component: DistanceLearning
  },
  {
    path: '/school_info/general',
    name: 'General',
    component: General
  },
  {
    path: '/school_info/collective',
    name: 'Collective',
    component: Collective
  },
  {
    path: '/school_info/education',
    name: 'Education',
    component: Education
  },
  {
    path: '/school_info/materialtechnicgiving',
    name: 'MaterialTechnicGiving',
    component: MaterialTechnicGiving
  },
  {
    path: '/school_info/MethodGivingStudy',
    name: 'MethodGivingStudy',
    component: MethodGivingStudy
  },
  {
    path: '/school_info/SecurityAntiterror',
    name: 'SecurityAntiterror',
    component: SecurityAntiterror
  },
  {
    path: '/victory/OtherVictory',
    name: 'OtherVictory',
    component: OtherVictory
  },
  {
    path: '/victory/SportVictory',
    name: 'SportVictory',
    component: SportVictory
  },
  {
    path: '/victory/StudentYearVictory',
    name: 'StudentYearVictory',
    component: StudentYearVictory
  },
  {
    path: '/useful/HotLines',
    name: 'HotLines',
    component: HotLines
  },
  {
    path: '/useful/LinksUse',
    name: 'LinksUse',
    component: LinksUse
  },
  {
    path: '/useful/InfoSecurity',
    name: 'InfoSecurity',
    component: InfoSecurity
  },
  {
    path: '/liblary/actualbooks',
    name: 'ActualBooks',
    component: ActualBooks
  },
  {
    path: '/liblary/general',
    name: 'LibGeneral',
    component: LibGeneral
  },
  {
    path: '/docs',
    name: 'Docs',
    component: Docs
  },
  {
    path: '/SchoolPaper',
    name: 'SchoolPaper',
    component: SchoolPaper
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router