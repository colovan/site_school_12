module.exports = {
    devServer: {
        proxy: {
            '^/api': {
                target: "http://school12batysk.ru",
                ws: true,
                changeOrigin: true
            },           
        }
    }
}