<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Content-Type: application/json");

include_once "config/core.php";
include_once "config/database.php";
@chmod('imgs/', 777);
@chmod('docs/',777);

if (!array_key_exists(2, explode('/',trim($_SERVER['REQUEST_URI'])))) {
    header($_SERVER['SERVER_PROTOCOL'].' 400 Bad Request');
        echo json_encode([
        'response' => false,
        'error' => 'Bad Request'
        ]);
        exit;
}
$action  = ucfirst(explode('/',trim($_SERVER['REQUEST_URI']))[2]);
$db = new Database();

if (!file_exists("class/$action.php"))
{
    header($_SERVER['SERVER_PROTOCOL'].' 400 Bad Request');
    echo json_encode([
        'response' => false,
        'error' => 'Bad Request'
        ]);
    exit;
} else {
    require_once 'class/'.$action.'.php';
    try {
        $class = new $action($db);  
    } catch (Exception $e) {
        header($_SERVER['SERVER_PROTOCOL'].' 400 Bad Request');
        echo json_encode([
        'response' => false,
        'error' => 'Bad Request'
        ]);
        exit;
    }
      
}
    



?>