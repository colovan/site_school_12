<?php

class Database
{
    private $db;
    private $params;

    public function __construct()
    {
        $this->params = include "db_params.php";
        $host = $this->params['host'];
        $db_name = $this->params['db_name'];
        $login = $this->params['login'];
        $password = $this->params['password'];

        try {
            $this->db = new PDO("mysql:host=$host;dbname=$db_name", $login, $password);
        } catch (Exception $e) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 521 Web Server Is Down ');
            echo json_encode([
                'error' => $e
            ]);
            exit;
        }
    }


    public function checkAdmin($data)
    {
        $stmt = $this->db->prepare("SELECT * FROM `admins` WHERE `login` = ?");
        $stmt->execute([$data['login']]);
        $admin_data = $stmt->fetch(PDO::FETCH_LAZY);
        if ($admin_data == false || $admin_data->password == NULL || !password_verify($data['password'], $admin_data->password))
            return false;
        return $admin_data->id;
    }


    public function checkToken($token)
    {
        $stmt = $this->db->prepare("SELECT `date_update` FROM `tokens` WHERE `value` = ?");
        $stmt->execute([$token]);
        $token_data = $stmt->fetch(PDO::FETCH_LAZY);
        if ($token_data === false || $token_data->date_update == NULL || time() - (int) $token_data->date_update > 3600)
            return false;
        return true;
    }


    public function createToken($user_id)
    {
        $token_value = password_hash($user_id . date("Ymdis") . time(), PASSWORD_DEFAULT);
        $stmt = $this->db->prepare("SELECT * FROM `tokens` WHERE `user_id` = ?");
        $stmt->execute([$user_id]);
        $token_data = $stmt->fetch(PDO::FETCH_LAZY);

        if ($token_data->value == NULL) {
            $stmt = $this->db->prepare("INSERT INTO `tokens`(`user_id`, `value`, `date_update`) VALUES (?,?,?)");
            $stmt->execute([$user_id, $token_value, time()]);
        } else {
            $stmt = $this->db->prepare("UPDATE `tokens` SET `value`=?,`date_update`=? WHERE `user_id` = ?");
            $stmt->execute([$token_value, time(), $user_id]);
        }
        return $token_value;
    }


    public function updateToken($token)
    {
        $stmt = $this->db->prepare("UPDATE `tokens` SET `date_update`= ? WHERE `value` = ?");
        $stmt->execute([time(), $token]);
    }

    public function depricateToken($token)
    {
        $stmt = $this->db->prepare("UPDATE `tokens` SET `date_update`= 0 WHERE `value` = ?");
        $stmt->execute([$token]);
    }


    public function checkExistDoc($data, $id = -1)
    {
        $stmt = $this->db->prepare("SELECT `id` FROM `docs` WHERE `title` = ? AND `category` = ?");
        $stmt->execute($data);
        $doc_data = $stmt->fetch(PDO::FETCH_LAZY);
        if ($doc_data === false || $doc_data['id'] == $id)
            return false;
        return true;
    }

    public function checkExistNew($title, $id = -1)
    {
        $stmt = $this->db->prepare("SELECT `id` FROM `news` WHERE `title` = ?");
        $stmt->execute([$title]);
        $new_data = $stmt->fetch(PDO::FETCH_LAZY);
        if ($new_data === false || $new_data['id'] == $id)
            return false;
        return true;
    }


    public function checkExistWorker($data, $id = -1)
    {
        $stmt = $this->db->prepare("SELECT `id` FROM `workers` WHERE `name` = ? AND `surname` = ? AND `lastname` = ? AND `category` = ?");
        $stmt->execute($data);
        $new_data = $stmt->fetch(PDO::FETCH_LAZY);
        if ($new_data === false || $new_data['id'] == $id)
            return false;
        return true;
    }


    public function checkExistBook($data, $id = -1)
    {
        $stmt = $this->db->prepare("SELECT `id` FROM `books` WHERE `title` = ? AND `authors` = ? AND `class` = ?");
        $stmt->execute($data);
        $new_data = $stmt->fetch(PDO::FETCH_LAZY);
        if ($new_data === false || $new_data['id'] == $id)
            return false;
        return true;
    }


    public function getDocs()
    {
        $data = $this->db->query("SELECT * FROM `docs`");
        if ($data != false)
            $data = $data->fetchAll(PDO::FETCH_ASSOC);
        else
            return false;
        return $data;
    }

    public function getBooks()
    {
        $data = $this->db->query("SELECT * FROM `books`");
        if ($data != false)
            $data = $data->fetchAll(PDO::FETCH_ASSOC);
        else
            return false;
        return $data;
    }


    public function getWorkers()
    {
        $data = $this->db->query("SELECT * FROM `workers` ORDER BY `surname`");
        if ($data != false)
            $data = $data->fetchAll(PDO::FETCH_ASSOC);
        else
            return false;
        return $data;
    }

    public function getNews()
    {
        $data = $this->db->query("SELECT * FROM `news` ORDER BY `date_create` DESC");
        if ($data != false)
            $data = $data->fetchAll(PDO::FETCH_ASSOC);
        else
            return false;
        foreach ($data as $key => $value) {
            $data[$key]['date_create'] = date("d-m-Y", (int) $value['date_create']);
        }
        return $data;
    }

    public function createDoc($data)
    {
        $stmt = $this->db->prepare("INSERT INTO `docs`(`title`, `category`, `file_way`) VALUES (?,?,?)");
        $stmt->execute($data);
        if ($stmt->errorCode() != 00000)
            return false;
        return true;
    }


    public function createNew($data)
    {
        $stmt = $this->db->prepare("INSERT INTO `news`(`title`, `content`, `img_way`, `date_create`) VALUES (?,?,?,?)");
        $stmt->execute($data);
        if ($stmt->errorCode() != 00000)
            return false;
        return true;
    }


    public function createWorker($data)
    {
        $stmt = $this->db->prepare("INSERT INTO `workers`(`name`, `surname`, `lastname`, `post`, `category`, `education`, `experience`, `img_way`) VALUES (?,?,?,?,?,?,?,?)");
        $stmt->execute($data);
        if ($stmt->errorCode() != 00000)
            return false;
        return true;
    }


    public function createBook($data)
    {
        $stmt = $this->db->prepare("INSERT INTO `books`(`title`, `authors`, `class`, `publishing`,  `img_way`) VALUES (?,?,?,?,?)");
        $stmt->execute($data);
        if ($stmt->errorCode() != 00000)
            return false;
        return true;
    }


    public function updateWorker($id, $data)
    {
        $stmt = $this->db->prepare("SELECT `img_way` FROM `workers` WHERE `id` = ?");
        $stmt->execute([$id]);
        $filename = $stmt->fetch(PDO::FETCH_LAZY)->img_way;
        array_push($data, $id);
        $stmt = $this->db->prepare("UPDATE `workers` SET `name`=?, `surname`=?, `lastname`=?, `post`=?, `category`=?, `education`=?, `experience`=?, `img_way`=? WHERE `id` = ?");
        $stmt->execute($data);
        if ($stmt->errorCode() != 00000)
            return false;
        if ($data[7] == null && $filename != null)
            unlink('imgs/' . $filename);
        return true;
    }


    public function updateBook($id, $data)
    {
        $stmt = $this->db->prepare("SELECT `img_way` FROM `books` WHERE `id` = ?");
        $stmt->execute([$id]);
        $filename = $stmt->fetch(PDO::FETCH_LAZY)->img_way;
        array_push($data, $id);
        $stmt = $this->db->prepare("UPDATE `books` SET `title`=?, `authors`=?, `class`=?, `publishing`=?, `img_way`=? WHERE `id` = ?");
        $stmt->execute($data);
        if ($stmt->errorCode() != 00000)
            return false;
        if ($data[4] == null && $filename != null)
            unlink('imgs/' . $filename);
        return true;
    }


    public function updateDoc($id, $data)
    {
        array_push($data, $id);
        $stmt = $this->db->prepare("UPDATE `docs` SET `title`=?,`category`=?,`file_way`=? WHERE `id` = ?");
        $stmt->execute($data);
        if ($stmt->errorCode() != 00000)
            return false;
        return true;
    }


    public function updateNew($id, $data)
    {
        array_push($data, $id);
        $stmt = $this->db->prepare("UPDATE `news` SET `title`=?,`content`=?,`img_way`=? WHERE `id` = ?");
        $stmt->execute($data);
        if ($stmt->errorCode() != 00000)
            return false;
        return true;
    }


    public function deleteDoc($id)
    {
        $stmt = $this->db->prepare("SELECT `file_way` FROM `docs` WHERE `id` = ?");
        $stmt->execute([$id]);
        $filename = $stmt->fetch(PDO::FETCH_LAZY)->file_way;
        $stmt = $this->db->prepare("DELETE FROM `docs` WHERE `id` = ?");
        $stmt->execute([$id]);
        if ($stmt->errorCode() != 00000)
            return false;
        unlink('docs/' . $filename);
        return true;
    }


    public function deleteNew($id)
    {
        $stmt = $this->db->prepare("SELECT `img_way` FROM `news` WHERE `id` = ?");
        $stmt->execute([$id]);
        $filename = $stmt->fetch(PDO::FETCH_LAZY)->img_way;
        $stmt = $this->db->prepare("DELETE FROM `news` WHERE `id` = ?");
        $stmt->execute([$id]);
        if ($stmt->errorCode() != 00000)
            return false;
        unlink('imgs/' . $filename);
        return true;
    }


    public function deleteWorker($id)
    {
        $stmt = $this->db->prepare("SELECT `img_way` FROM `workers` WHERE `id` = ?");
        $stmt->execute([$id]);
        $filename = $stmt->fetch(PDO::FETCH_LAZY)->img_way;
        $stmt = $this->db->prepare("DELETE FROM `workers` WHERE `id` = ?");
        $stmt->execute([$id]);
        if ($stmt->errorCode() != 00000)
            return false;
        if ($filename != null)
            unlink('imgs/' . $filename);
        return true;
    }


    public function deleteBook($id)
    {
        $stmt = $this->db->prepare("SELECT `img_way` FROM `books` WHERE `id` = ?");
        $stmt->execute([$id]);
        $filename = $stmt->fetch(PDO::FETCH_LAZY)->file_way;
        $stmt = $this->db->prepare("DELETE FROM `books` WHERE `id` = ?");
        $stmt->execute([$id]);
        if ($stmt->errorCode() != 00000)
            return false;
        if ($filename != null)
            unlink('imgs/' . $filename);
        return true;
    }
}
