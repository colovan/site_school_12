<?php
include_once "Api.php";

class Sign extends Api
{

    public function __construct($db)
    {
        $action = $this->getAction();
        if ($action == null)
            $this->response(null, 400);
        $this->$action($db);
    }

    public function viewAction($db) {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        if ($db->checkToken(getallheaders()['Token']))
        {
            $this->response(null,200);
        }
        else
        {
            $this->response(null,403);
        }
    }


    public function createAction($db) {
        if (!array_key_exists('login', $_POST) || !array_key_exists('password', $_POST)) 
            $this->response(null, 401);
        $login = trim( htmlspecialchars( strip_tags($_POST['login']) ) );
        $password = trim( htmlspecialchars( strip_tags($_POST['password']) ) ); 
        if (!preg_match('/^[a-zA-Z0-9_-]{5,18}$/', $login) || !preg_match('/^[a-zA-Z0-9_-]{5,18}$/', $password)) {
            $this->response(null, 401); 
        }
        $id = $db->checkAdmin([
            "login" => $login, 
            "password" => $password
        ]);
        if ($id !== false) {
            $token = $db->createToken($id);
            $this->response(['token'=>$token], 200);
        } else {
            $this->response(null, 401);
        }
    }


    public function updateAction($db) {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        $db->updateToken($token);
        $this->response(null,200);

    }

    public function deleteAction($db) {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        $db->depricateToken($token);
        $this->response(null,200);

    }
}