<?php
include_once "Api.php";

class Workers extends Api
{
    public function __construct($db)
    {
        $action = $this->getAction();
        if ($action == null)
            $this->response(null, 400);
        $this->$action($db);
    }


    public function viewAction($db)
    {
        $data = $db->getWorkers();
        if ($data !== false)
            $this->response($data, 200);
        else
            $this->response(null, 400);
    }


    public function createAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        if (
            !array_key_exists('name', $_POST) ||
            !array_key_exists('surname', $_POST) ||
            !array_key_exists('lastname', $_POST) ||
            !array_key_exists('post', $_POST) ||
            !array_key_exists('category', $_POST)
        )
            $this->response(null, 401);

        if (!array_key_exists('file', $_FILES))
            $file = null;
        else
            $file = $_FILES['file'];

        $name = trim(strip_tags($_POST['name']));
        $surname = trim(strip_tags($_POST['surname']));
        $lastname = trim(strip_tags($_POST['lastname']));
        $post = trim(strip_tags($_POST['post']));
        $category = trim(strip_tags($_POST['category']));
        $education = trim(strip_tags($_POST['education']));
        $education = $education == "null" ? null : $education;
        $experience = trim(strip_tags($_POST['experience']));
        $experience = $experience == "null" ? null : $experience;
        $filename = null;


        if (
            !preg_match('/^.{2,255}$/', $name) ||
            !preg_match('/^.{2,255}$/', $surname) ||
            !preg_match('/^.{2,255}$/', $lastname) ||
            !preg_match('/^.{2,255}$/', $post) ||
            !preg_match('/^.{2,255}$/', $category)
        )
            $this->response(null, 401);

        if ($db->checkExistWorker([$name, $surname, $lastname, $category]))
            $this->response(null, 401);

        if ($file != null) {
            if (
                !in_array($file['type'], ["image/jpeg", "image/png", "image/gif"]) ||
                $file['size'] > 10000000
            )
                $this->response(null, 401);
            $filename = 'workers' . time() . "." . explode('/', $file['type'])[1];
            if (!move_uploaded_file($file['tmp_name'], 'imgs/' . $filename))
                $this->response(null, 401);
        }


        if ($db->createWorker([$name, $surname, $lastname, $post, $category, $education, $experience, $filename]))
            $this->response(null, 201);
        else
            $this->response(null, 500);
    }


    public function updateAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        if (
            !array_key_exists('name', $_POST) ||
            !array_key_exists('surname', $_POST) ||
            !array_key_exists('lastname', $_POST) ||
            !array_key_exists('post', $_POST) ||
            !array_key_exists('category', $_POST)
        )
            $this->response(null, 401);

        if (!array_key_exists('file', $_FILES)) {
            $file = null;
        } else {
            $file = $_FILES['file'];
        }

        if ($file == null && $_POST['file'] == 'null') {
            $filename = null;
        }
        else {
            $filename = trim(strip_tags($_POST['filename']));
            $filename = $filename == "null" ? null : $filename;  
        }
        


        $name = trim(strip_tags($_POST['name']));
        $surname = trim(strip_tags($_POST['surname']));
        $lastname = trim(strip_tags($_POST['lastname']));
        $post = trim(strip_tags($_POST['post']));
        $category = trim(strip_tags($_POST['category']));
        $education = trim(strip_tags($_POST['education']));
        $education = $education == "null" ? null : $education;
        $experience = trim(strip_tags($_POST['experience']));
        $experience = $experience == "null" ? null : $experience;



        $id = array_key_exists(3, explode('/', trim($_SERVER['REQUEST_URI']))) ? (int) explode('/', trim($_SERVER['REQUEST_URI']))[4] : -1;
        if ($id === false || !is_numeric(($id)) || $id == -1)
            $this->response(null, 400);


        if (
            !preg_match('/^.{2,255}$/', $name) ||
            !preg_match('/^.{2,255}$/', $surname) ||
            !preg_match('/^.{2,255}$/', $lastname) ||
            !preg_match('/^.{2,255}$/', $post) ||
            !preg_match('/^.{2,255}$/', $category)
        )
            $this->response(null, 401);

        if ($db->checkExistWorker([$name, $surname, $lastname, $category], $id))
            $this->response(null, 401);

        if ($file != null) {
            if (
                !in_array($file['type'], ["image/jpeg", "image/png", "image/gif"]) ||
                $file['size'] > 10000000
            )
                $this->response(null, 401);
            if ($filename != null)
                unlink('imgs/' . $filename);
            $filename = 'workers' . time() . "." . explode('/', $file['type'])[1];
            if (!move_uploaded_file($file['tmp_name'], 'imgs/' . $filename))
                $this->response(null, 401);
        }

        if ($db->updateWorker($id, [$name, $surname, $lastname, $post, $category, $education, $experience, $filename]))
            $this->response(null, 200);
        else
            $this->response(null, 500);
    }


    public function deleteAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        $id = array_key_exists(3, explode('/', trim($_SERVER['REQUEST_URI']))) ? (int) explode('/', trim($_SERVER['REQUEST_URI']))[4] : -1;
        if ($id === false || !is_numeric(($id)) || $id == -1)
            $this->response(null, 400);

        if ($db->deleteWorker($id))
            $this->response(null, 200);
        else
            $this->response(null, 500);
    }
}
