<?php
include_once "Api.php";

class News extends Api
{
    public function __construct($db)
    {
        $action = $this->getAction();
        if ($action == null)
            $this->response(null, 400);
        $this->$action($db);
    }


    public function viewAction($db)
    {
        $data = $db->getNews();
        if ($data !== false)
            $this->response($data, 200);
        else
            $this->response(null, 400);
    }


    public function createAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        if (!array_key_exists('title', $_POST) || !array_key_exists('content', $_POST))
            $this->response(null, 401);

        if (!array_key_exists('file', $_FILES))
            $this->response(null, 401);

        $title = trim(strip_tags($_POST['title']));
        $content = trim(strip_tags($_POST['content']));
        $file = $_FILES['file'];
        $filename = 'news' . time() . "." . explode('/', $file['type'])[1];
        $date_create = time();
        if (
            !(strlen($title) > 2 && strlen($title) < 255) ||
            !(strlen($content) > 10 && strlen($content) < 6768) ||
            !in_array($file['type'], ["image/jpeg", "image/png", "image/gif"]) ||
            $file['size'] > 10000000
        )
            $this->response(null, 401);


        if ($db->checkExistNew($title))
            $this->response(null, 401);

        if (!move_uploaded_file($file['tmp_name'], 'imgs/' . $filename))
            $this->response(null, 401);

        if ($db->createNew([$title, $content, $filename, $date_create]))
            $this->response(null, 201);
        else
            $this->response(null, 500);
    }


    public function updateAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        if (!array_key_exists('title', $_POST) || !array_key_exists('content', $_POST) || !array_key_exists('newfile', $_POST))
            $this->response(null, 401);

        $id = array_key_exists(3, explode('/', trim($_SERVER['REQUEST_URI']))) ? (int) explode('/', trim($_SERVER['REQUEST_URI']))[4] : -1;
        if ($id === false || !is_numeric(($id)) || $id == -1)
            $this->response(null, 400);

        $newfile = $_POST['newfile'] == 'false' ? false : true;
        $filename = $_POST['filename'];
        if ($newfile) {
            if (!array_key_exists('file', $_FILES))
                $this->response(null, 401);
            $file = $_FILES['file'];
            if (!in_array($file['type'], ["image/jpeg", "image/png", "image/gif"]) || $file['size'] > 10000000)
                $this->response(null, 401);
            unlink('imgs/' . $filename);
            $filename = 'news' . time() . "." . explode('/', $file['type'])[1];
            if (!move_uploaded_file($file['tmp_name'], 'imgs/' . $filename))
                $this->response(null, 401);
        }

        $title = trim(strip_tags($_POST['title']));
        $content = trim(strip_tags($_POST['content']));
        if (
            !(strlen($title) > 2 && strlen($title) < 255) ||
            !(strlen($content) > 10 && strlen($content) < 6768)
        )
            $this->response(null, 401);

        if ($db->checkExistNew($title, $id))
            $this->response(null, 401);

        if ($db->updateNew($id, [$title, $content, $filename]))
            $this->response(null, 200);
        else
            $this->response(null, 500);
    }


    public function deleteAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        $id = array_key_exists(3, explode('/', trim($_SERVER['REQUEST_URI']))) ? (int) explode('/', trim($_SERVER['REQUEST_URI']))[4] : -1;
        if ($id === false || !is_numeric(($id)) || $id == -1)
            $this->response(null, 400);

        if ($db->deleteNew($id))
            $this->response(null, 200);
        else
            $this->response(null, 500);
    }
}
