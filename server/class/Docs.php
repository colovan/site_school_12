<?php
include_once "Api.php";

class Docs extends Api
{
    public function __construct($db)
    {
        $action = $this->getAction();
        if ($action == null)
            $this->response(null, 400);
        $this->$action($db);
    }


    public function viewAction($db)
    {
        $data = $db->getDocs();
        if ($data !== false)
        $this->response($data, 200);
        else
        $this->response(null, 400);
    }


    public function createAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        if (!array_key_exists('title', $_POST) || !array_key_exists('category', $_POST))
            $this->response(null, 401);

        if (!array_key_exists('file', $_FILES))
            $this->response(null, 401);

        $title = trim(strip_tags($_POST['title']));
        $category = trim(strip_tags($_POST['category']));
        $filename = time() . ".pdf";
        $file = $_FILES['file'];

        if (!preg_match('/^.{2,255}$/', $title) || !preg_match('/^.{2,255}$/', $category) || $file['type'] != "application/pdf" || $file['size'] > 50000000)
            $this->response(null, 401);

        if ($db->checkExistDoc([$title, $category]))
            $this->response(null, 401);

        if (!move_uploaded_file($file['tmp_name'], 'docs/' . $filename))
            $this->response(null, 401);

        if ($db->createDoc([$title, $category, $filename]))
            $this->response(null, 201);
        else
            $this->response(null, 500);
    }


    public function updateAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        if (!array_key_exists('title', $_POST) || !array_key_exists('category', $_POST) || !array_key_exists('newfile', $_POST))
            $this->response(null, 401);

        $id = array_key_exists(3, explode('/', trim($_SERVER['REQUEST_URI']))) ? (int) explode('/', trim($_SERVER['REQUEST_URI']))[4] : -1;
        if ($id === false || !is_numeric(($id)) || $id == -1)
            $this->response(null, 400);

        $newfile = $_POST['newfile'] == 'false' ? false : true;
        $filename = $_POST['filename'];
        if ($newfile) {
            if (!array_key_exists('file', $_FILES))
                $this->response(null, 401);
            $file = $_FILES['file'];
            if ($file['type'] != "application/pdf" || $file['size'] > 50000000)
                $this->response(null, 401);
            unlink('docs/' . $filename);
            $filename = time() . ".pdf";
            if (!move_uploaded_file($file['tmp_name'], 'docs/' . $filename))
                $this->response(null, 401);
        }

        $title = trim(strip_tags($_POST['title']));
        $category = trim(strip_tags($_POST['category']));
        if (!preg_match('/^.{2,255}$/', $title) || !preg_match('/^.{2,255}$/', $category))
            $this->response(null, 401);
        if ($db->checkExistDoc([$title, $category], $id))
            $this->response(null, 401);

        if ($db->updateDoc($id, [$title, $category, $filename]))
            $this->response(null, 200);
        else
            $this->response(null, 500);
    }


    public function deleteAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        $id = array_key_exists(3, explode('/', trim($_SERVER['REQUEST_URI']))) ? (int) explode('/', trim($_SERVER['REQUEST_URI']))[4] : -1;
        if ($id === false || !is_numeric(($id)) || $id == -1)
            $this->response(null, 400);

        if ($db->deleteDoc($id))
            $this->response(null, 200);
        else
            $this->response(null, 500);
    }
}
