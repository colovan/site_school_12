<?php
include_once "Api.php";

class Books extends Api
{
    public function __construct($db)
    {
        $action = $this->getAction();
        if ($action == null)
            $this->response(null, 400);
        $this->$action($db);
    }


    public function viewAction($db)
    {
        $data = $db->getBooks();
        if ($data !== false)
            $this->response($data, 200);
        else
            $this->response(null, 400);
    }


    public function createAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        if (
            !array_key_exists('title', $_POST) ||
            !array_key_exists('authors', $_POST) ||
            !array_key_exists('class', $_POST) ||
            !array_key_exists('publishing', $_POST)
        )
            $this->response(null, 401);

        if (!array_key_exists('file', $_FILES))
            $file = null;
        else
            $file = $_FILES['file'];

        $title = trim(strip_tags($_POST['title']));
        $authors = trim(strip_tags($_POST['authors']));
        $class = trim(strip_tags($_POST['class']));
        $publishing = trim(strip_tags($_POST['publishing']));
        $filename = null;


        if (
            !preg_match('/^.{2,255}$/', $title) ||
            !preg_match('/^.{2,255}$/', $authors) ||
            !preg_match('/^.{2,255}$/', $class) ||
            !preg_match('/^.{2,255}$/', $publishing)
        )
            $this->response(null, 401);

        if ($db->checkExistBook([$title, $authors, $class]))
            $this->response(null, 401);

        if ($file != null) {
            if (
                !in_array($file['type'], ["image/jpeg", "image/png", "image/gif"]) ||
                $file['size'] > 10000000
            )
                $this->response(null, 401);
            $filename = 'books' . time() . "." . explode('/', $file['type'])[1];
            if (!move_uploaded_file($file['tmp_name'], 'imgs/' . $filename))
                $this->response(null, 401);
        }


        if ($db->createBook([$title, $authors, $class, $publishing, $filename]))
            $this->response(null, 201);
        else
            $this->response(null, 500);
    }


    public function updateAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        if (
            !array_key_exists('title', $_POST) ||
            !array_key_exists('authors', $_POST) ||
            !array_key_exists('class', $_POST) ||
            !array_key_exists('publishing', $_POST)
        )
            $this->response(null, 401);

        if (!array_key_exists('file', $_FILES)) {
            $file = null;
        } else {
            $file = $_FILES['file'];
        }

        if ($file == null && $_POST['file'] == 'null') {
            $filename = null;
        }
        else {
            $filename = trim(strip_tags($_POST['filename']));
            $filename = $filename == "null" ? null : $filename;  
        }
        

        $title = trim(strip_tags($_POST['title']));
        $authors = trim(strip_tags($_POST['authors']));
        $class = trim(strip_tags($_POST['class']));
        $publishing = trim(strip_tags($_POST['publishing']));

        $id = array_key_exists(3, explode('/', trim($_SERVER['REQUEST_URI']))) ? (int) explode('/', trim($_SERVER['REQUEST_URI']))[4] : -1;
        if ($id === false || !is_numeric(($id)) || $id == -1)
            $this->response(null, 400);

        if (
            !preg_match('/^.{2,255}$/', $title) ||
            !preg_match('/^.{2,255}$/', $authors) ||
            !preg_match('/^.{2,255}$/', $class) ||
            !preg_match('/^.{2,255}$/', $publishing)
        )
            $this->response(null, 401);

        if ($db->checkExistBook([$title, $authors, $class], $id))
            $this->response(null, 401);

        if ($file != null) {
            if (
                !in_array($file['type'], ["image/jpeg", "image/png", "image/gif"]) ||
                $file['size'] > 10000000
            )
                $this->response(null, 401);
            if ($filename != null)
                unlink('imgs/' . $filename);
            $filename = 'books' . time() . "." . explode('/', $file['type'])[1];
            if (!move_uploaded_file($file['tmp_name'], 'imgs/' . $filename))
                $this->response(null, 401);
        }

        if ($db->updateBook($id, [$title, $authors, $class, $publishing, $filename]))
            $this->response(null, 200);
        else
            $this->response(null, 500);
    }



    public function deleteAction($db)
    {
        if (!array_key_exists('Token', getallheaders()))
            $this->response(null, 400);
        $token = getallheaders()['Token'];
        if (!$db->checkToken($token))
            $this->response(null, 403);

        $id = array_key_exists(3, explode('/', trim($_SERVER['REQUEST_URI']))) ? (int) explode('/', trim($_SERVER['REQUEST_URI']))[4] : -1;
        if ($id === false || !is_numeric(($id)) || $id == -1)
            $this->response(null, 400);

        if ($db->deleteBook($id))
            $this->response(null, 200);
        else
            $this->response(null, 500);
    }
}
