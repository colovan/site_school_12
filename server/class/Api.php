<?php

class Api {
    protected function response($data = NULL, $status = 500) {
        header($_SERVER['SERVER_PROTOCOL'] ." " . $status . " " . $this->requestStatus($status));
        echo json_encode($data);
        exit;
    }

    private function requestStatus($code) {
        $status = array(
            200 => 'OK',
            201 => 'Added',
            400 => 'Bad Request',
            401  => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error'
        );
        return ($status[$code])?$status[$code]:$status[500];
    }

    protected function getAction()
    {
        $method = array_key_exists(3, explode('/',trim($_SERVER['REQUEST_URI']))) ?
        explode('/',trim($_SERVER['REQUEST_URI']))[3] :
        '';
        switch ($method) {
            case '':
                return 'viewAction';
                break;
            case 'create':
                return 'createAction';
                break;
            case 'update':
                return 'updateAction';
                break;
            case 'delete':
                return 'deleteAction';
                break;
            default:
                return null;
        }
    }
}